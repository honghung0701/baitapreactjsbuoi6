import React, { Component } from "react";
import { connect } from "react-redux";
import { changeTangGiamSoLuongAction } from "./redux/actions/shoeActions";

class Cart extends Component {
  renderTbody = () => {
    return this.props.cart?.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img
              style={{
                width: "50px",
              }}
              src={item.image}
              alt=""
            />
          </td>
          <td>{item.price * item.number} $</td>
          <td>
            <button 
            onClick={() => {
              this.props.handleTangGiamSoLuong(item.id, -1);
            }}
            className="btn btn-danger">-</button>
            <span className="mx-2">{item.number}</span>
            <button 
            onClick={() => {
              this.props.handleTangGiamSoLuong(item.id, +1);
            }}
            className="btn btn-success">+</button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Img</th>
            <th>Price</th>
            <th>Quantity ( số lượng )</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  }
}
let mapDispatchToProps = (dispatch) => {  
  return {
    handleTangGiamSoLuong:(id, soLuong) => {
      // let action = {
      //   type: "TANG_GIAM_SO_LUONG",
      //   payload: {
      //     idShoe: id,
      //     soLuong: soLuong,
      //   },
      // };
      dispatch(changeTangGiamSoLuongAction(id,soLuong));
    }
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Cart);

