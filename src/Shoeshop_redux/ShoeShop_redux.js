import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";

export default class ShoeShop_redux extends Component {
  render() {
    return (
      <div className="container py-5">
        <Cart />
        <ListShoe />
        <DetailShoe  />
      </div>
    );
  }
}
