import { ADD_TO_CART, TANG_GIAM_SO_LUONG, XEM_CHI_TIET } from "../constants/shoeConstant"

export const changeDetailAction= (value) => {
    return {
        type: XEM_CHI_TIET,
        payload: value,
    }
}
export const changeAddToCartAction =(value) => {
    return {
        type: ADD_TO_CART,
        payload: value,
    }
}
export const changeTangGiamSoLuongAction = (id,value) => {
    return {
        type: TANG_GIAM_SO_LUONG,
        payload: {
            idShoe: id,
            soLuong: value,
        }
    }
}