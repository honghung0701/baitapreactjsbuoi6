import logo from './logo.svg';
import './App.css';
import ShoeShop_redux from './Shoeshop_redux/ShoeShop_redux';

function App() {
  return (
    <div className="App">
      <ShoeShop_redux />
    </div>
  );
}

export default App;
